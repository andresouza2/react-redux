import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = [
  { name: 'JAVA', favorite: false },
  { name: 'C#', favorite: false },
  { name: 'PHP', favorite: false },
  { name: 'JAVASCRIPT', favorite: false }
]

const sliceLanguages = createSlice({
  name: 'languages',
  initialState: INITIAL_STATE,
  reducers: {
    addLanguages(state, { payload }) {
      return [...state, { name: payload, favorite: false }]
    },
    toFavorite(state, { payload }) {
      return state.map(st => st.name === payload ? { ...st, favorite: !st.favorite } : st)
    }
  }
})

export default sliceLanguages.reducer;
export const { addLanguages, toFavorite } = sliceLanguages.actions;

export const useLanguages = (state) => {
  return state.languages
}